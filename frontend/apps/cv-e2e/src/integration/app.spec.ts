import { getGreeting } from '../support/app.po';

describe('cv', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to cv!');
  });
});
