import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '@my/auth';
import {
  HomeLayoutComponent,
  LoginLayoutComponent,
  LoginComponent,
  RegisterComponent
} from '@my/ui';

const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    data: {
      links: [
        {name: 'Documents', path: 'docs'}
      ]
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'docs',
        loadChildren: () => import('@my/docs').then(m => m.DocsModule)
      },
      {
        path: '',
        redirectTo: '/docs',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
