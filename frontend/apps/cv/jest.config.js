module.exports = {
  name: 'cv',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/cv',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
