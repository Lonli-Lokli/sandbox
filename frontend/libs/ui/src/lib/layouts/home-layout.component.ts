import { Component } from '@angular/core';
import { NavBar } from '../shared/navbar.data';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'my-ui-home-layout',
  template: `
    <my-ui-header [activeNavBars]="navData"></my-ui-header>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class HomeLayoutComponent {
  navData: NavBar[];

  constructor(routeData: ActivatedRoute) {
    this.navData = routeData.snapshot.data.links;
  }
}
