import { Component, OnInit, Input } from '@angular/core';
import { NavBar } from '../shared/navbar.data';
import { AuthService } from '@my/auth';

@Component({
  selector: 'my-ui-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() activeNavBars: NavBar[];

  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  logout() {
    this.auth.logout();
  }

}
