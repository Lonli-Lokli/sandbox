import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpXhrBackend, HttpRequest, HttpResponse } from '@angular/common/http';
import { HostSettings } from '@my/core';
import { UserData } from './user.data';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private tokenKey = 'CV__TOKEN';
  private tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);

  get isLoggedIn$(): Observable<boolean> {
    return this.token$.pipe(
      map(token => !!token ? true : false)
    )
  }

  get token$(): Observable<string> {
    return this.tokenSubject.asObservable();
  }

  constructor(
    private backend: HttpXhrBackend, // do not use HttpClient due to possible issues with interceptors
    private host: HostSettings,
    private router: Router
  ) {
    // just PoC, bad prqctice
    let token: string = localStorage.getItem(this.tokenKey);
    if (!!token) {
      this.tokenSubject.next(token);
    }

  }

  login(user: UserData) {
    let req: HttpRequest<any> = new HttpRequest('PUT',
      this.host.endpoint + '/api/users/login', {
        Name: user.Name,
        Password: user.Password
      }, {responseType: 'text'});
    this.backend.handle(req).subscribe((resp: HttpResponse<any>) =>this.processResponse(resp));
  }

logout() {
    localStorage.removeItem(this.tokenKey);
    this.tokenSubject.next(undefined);
    this.router.navigate(['/login']);
  }

  register(userData: UserData) {
    let req: HttpRequest<any> = new HttpRequest('PUT',
      this.host.endpoint + '/api/users', {
        Name: userData.Name,
        Password: userData.Password
      }, {responseType: 'text'});
    this.backend.handle(req).subscribe((resp: HttpResponse<any>) => this.processResponse(resp));
  }

  private processResponse(resp: HttpResponse<any>) {
    if (!resp.ok) return;
    let tokenValue = resp.body;
    localStorage.setItem(this.tokenKey, tokenValue);
    this.tokenSubject.next(tokenValue);
    this.router.navigate(['/']);
  }
}
