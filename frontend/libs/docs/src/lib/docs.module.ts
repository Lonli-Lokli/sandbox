import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocsRoutingModule } from './docs-routing.module';
import { DocsListComponent } from './docs/docs-list/docs-list.component';
import { DocsComponent } from './docs/docs.component';
import { UiMaterialModule } from '@my/ui';
import { DocumentsDataService } from './shared/documents-data.service';
import { DocItemComponent } from './docs/doc-item/doc-item.component';

@NgModule({
  imports: [
    CommonModule,
    DocsRoutingModule,
    UiMaterialModule
  ],
  declarations: [
    DocsComponent,
    DocsListComponent,
    DocItemComponent
  ],
  providers: [
    DocumentsDataService
  ]
})
export class DocsModule {}
