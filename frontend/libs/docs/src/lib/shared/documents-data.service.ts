import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { DocumentData } from './document-data.model';
import { HostSettings } from '@my/core';
import { HttpClient } from '@angular/common/http';
import { catchError, mapTo, tap, filter } from 'rxjs/operators';

@Injectable()
export class DocumentsDataService {

constructor(private http: HttpClient, private host: HostSettings) { }

  public getDocuments(): Observable<DocumentData[]> {
    return this.http.get<DocumentData[]>(this.buildUrl('Documents'));
  }

  public saveDocument(file: File) {
    let formData = new FormData();
    formData.append('file', file);
    return this.http.put(this.buildUrl('Documents'), formData).pipe(
      catchError(() => of(false)),
      mapTo(true)
    )
  }

  public downloadDocument(doc: DocumentData) {
    return this.http.get(this.buildUrl('Documents', doc.id), { responseType: 'blob' as 'json'}).pipe(
      catchError(() => of(undefined)),
      filter((i => !!i)),
      tap((blob) => {
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = doc.name;
        link.click();
      }),
      mapTo(true)
    );
  }

  public removeDocument(id) {
    return this.http.delete(this.buildUrl('Documents', id)).pipe(
      catchError(() => of(false)),
      mapTo(true)
    );
  }

  private buildUrl(...relative): string {
    return this.host.endpoint + '/api/' + relative.join('/');
  }
}
