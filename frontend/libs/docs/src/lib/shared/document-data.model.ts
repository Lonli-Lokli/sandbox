export interface DocumentData {
  author: string;
  name: string;
  id: string;
}
