import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocsComponent } from './docs/docs.component';
import { DocsListComponent } from './docs/docs-list/docs-list.component';
import { DocItemComponent } from './docs/doc-item/doc-item.component';

const routes: Routes = [
  {
    path: '',
    component: DocsComponent,
    children: [
      {
        path: ':id',
        component: DocItemComponent
      },
      {
        path: '',
        component: DocsListComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocsRoutingModule { }
