import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DocumentData } from '../../shared/document-data.model';

@Component({
  selector: 'my-doc-item',
  templateUrl: './doc-item.component.html',
  styleUrls: ['./doc-item.component.scss']
})
export class DocItemComponent implements OnInit {

  @Input() doc: DocumentData;
  @Output() remove: EventEmitter<void> = new EventEmitter<void>();
  @Output() download: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  public removeItem() {
    this.remove.emit();
  }

  public downloadItem() {
    this.download.emit();
  }
}
