import { Component } from '@angular/core';
import { DataState } from '../../shared/data-state.model';
import { DocumentsDataService } from '../../shared/documents-data.service';
import { DocumentData } from '../../shared/document-data.model';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { tap, catchError, switchMap, take, filter } from 'rxjs/operators';

@Component({
  selector: 'my-docs-list',
  templateUrl: './docs-list.component.html',
  styleUrls: ['./docs-list.component.scss']
})
export class DocsListComponent  {

  public dataState: DataState = DataState.Loading;
  public StateType = DataState;
  public datasource$: Observable<DocumentData[]>;

  private refreshRequired: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(private dataService: DocumentsDataService) {
    this.datasource$ = this.refreshRequired.pipe(
      filter(v => !!v),
      switchMap(() => this.dataService.getDocuments().pipe(
      catchError((err) => {
        this.dataState = DataState.Failed;
        return throwError(err)
      }),
      tap(() => this.dataState = DataState.Loaded)
    )));
  }

  upload(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.dataService.saveDocument(fileList[0]).pipe(
        take(1),
        tap((res) => {
          this.refreshRequired.next(res);
          event.target.value = ''; // to allow reselect same file
        })).subscribe();
    }
  }

  removeDocument(doc: DocumentData) {
    this.dataService.removeDocument(doc.id).pipe(
      take(1),
      tap((res) => this.refreshRequired.next(res))
    ).subscribe();
  }

  downloadDocument(doc: DocumentData) {
    this.dataService.downloadDocument(doc).pipe(
      take(1)).subscribe();
  }
}
