﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using CV.Domain.Services;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using Moq;
using Xunit;

namespace CV.Domain.Tests
{
    /// <summary>
    /// Test suite for <see cref="UserService"/>.
    /// </summary>
    public class UserServiceTests
    {
        private Mock<IUserRepository> _repository = new Mock<IUserRepository>();
        private Mock<IPasswordHasher> _passwordHasher = new Mock<IPasswordHasher>();

        /// <summary>
        /// Ensures wrong input data handled
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        [Theory]
        [InlineData(null, null)]
        [InlineData(null, "")]
        [InlineData(null, " ")]
        [InlineData("", " ")]
        [InlineData(" ", " ")]
        public void UserService_Create_Invalid_Test(string user, string password)
        {
            // Arrange
            var tc = CreateTestCandidate();

            // Act
            var results = tc.CreateUser(user, password);

            // Assert
            Assert.NotEmpty(results);
            _repository.Verify(r => r.Create(It.IsAny<User>()), Times.Never());
        }

        /// <summary>
        /// Ensures duplicated users cannot be created.
        /// </summary>
        [Fact]
        public void UserService_Cannot_Create_Duplicated_User_Test()
        {
            // Arrange
            var tc = CreateTestCandidate();
            string userName = "testUser";
            _repository.Setup(i => i.IsNameAvailable(userName)).Returns(false);

            // Act
            var results = tc.CreateUser(userName, "any");

            // Assert
            Assert.NotEmpty(results);
            _repository.Verify(r => r.Create(It.IsAny<User>()), Times.Never());
            _repository.Verify(r => r.IsNameAvailable(userName), Times.Once());
        }

        /// <summary>
        /// Ensures duplicated users cannot be created.
        /// </summary>
        [Fact]
        public void UserService_Password_Hashed()
        {
            // Arrange
            var tc = CreateTestCandidate();
            string userName = "testUser";
            string password = "testPassword";
            string hash = "testHash";
            _repository.Setup(i => i.IsNameAvailable(userName)).Returns(true);
            _passwordHasher.Setup(i => i.Hash(password)).Returns(hash);

            // Act
            var results = tc.CreateUser(userName, password);

            // Assert
            Assert.Empty(results);
            _repository.Verify(r => r.Create(It.Is<User>(u => u.PasswordHash == hash)), Times.Once());
        }

        private UserService CreateTestCandidate()
        {
            return new UserService(_repository.Object, _passwordHasher.Object);
        }
    }
}
