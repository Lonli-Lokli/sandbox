﻿// Copyright (c) 2019 under MIT license.

using System;
using AutoMapper;
using CV.Domain.Models;
using CV.Repository.LiteDb.Entities;

namespace CV.Repository.LiteDb
{
    /// <summary>
    /// Class AutoMapperSqlToDmProfile.
    /// </summary>
    /// <seealso cref="Profile" />
    public class AutoMapperDbToDmProfile : Profile
    {
        /// <inheritdoc />
        public AutoMapperDbToDmProfile() : this("LiteDbToDmProfile")
        {
        }

        /// <inheritdoc />
        protected AutoMapperDbToDmProfile(string profileName) : base(profileName)
        {
            CreateMap<DocumentEntity, Document>();
            CreateMap<UserEntity, User>();
        }
    }

    /// <summary>
    /// Class AutoMapperSqlToDmProfile.
    /// </summary>
    /// <seealso cref="Profile" />
    public class AutoMapperDmToDbProfile : Profile
    {
        /// <inheritdoc />
        public AutoMapperDmToDbProfile() : this("DmToLiteDbProfile")
        {
        }

        /// <inheritdoc />
        protected AutoMapperDmToDbProfile(string profileName) : base(profileName)
        {
            CreateMap<User, UserEntity>();
            CreateMap<Document, DocumentEntity>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => Guid.NewGuid().ToString()));
        }
    }
}
