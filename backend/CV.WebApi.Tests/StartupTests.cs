﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using CV.Domain.Configurations;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb.Repositories;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.RazorPages.Internal;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.ObjectPool;
using Moq;
using Swashbuckle.AspNetCore.Swagger;
using Xunit;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;
using IPasswordHasher = CV.Domain.Services.Abstractions.IPasswordHasher;

namespace CV.WebApi.Tests
{
    /// <summary>
    /// Class StartupTests
    /// </summary>
    public class StartupTests
    {
        /// <summary>
        /// Defines the test method Startup_Configure.
        /// </summary>
        [Fact]
        public void Startup_Configure()
        {
            // Arrange

            Func<string, IConfigurationSection> mockConfiguratonValue = propValue =>
            {
                var mock = new Mock<IConfigurationSection>();
                mock.Setup(i => i.Value).Returns(propValue);
                return mock.Object;
            };

            var dbSettingsMock = new Mock<IConfigurationSection>();
            dbSettingsMock.Setup(i => i.GetChildren())
                .Returns(new List<IConfigurationSection>() { new Mock<IConfigurationSection>().Object });
            dbSettingsMock.Setup(i => i.GetSection(nameof(LiteDbConfiguration.DbPath)))
                .Returns(mockConfiguratonValue("dbPath"));
            dbSettingsMock.Setup(i => i.GetSection(nameof(LiteDbConfiguration.Password)))
                .Returns(mockConfiguratonValue("password"));

            var hashingMock = new Mock<IConfigurationSection>();
            hashingMock.Setup(i => i.GetChildren())
                .Returns(new List<IConfigurationSection>() { new Mock<IConfigurationSection>().Object });
            hashingMock.Setup(i => i.GetSection(nameof(HashingOptions.Iterations)))
                .Returns(mockConfiguratonValue("10000"));

            var securityMock = new Mock<IConfigurationSection>();
            securityMock.Setup(i => i.GetChildren())
                .Returns(new List<IConfigurationSection>() { new Mock<IConfigurationSection>().Object });
            securityMock.Setup(i => i.GetSection(nameof(SecurityOptions.Secret)))
                .Returns(mockConfiguratonValue(new string('*', 30)));

            var configurationMock = new Mock<IConfigurationRoot>();
            configurationMock.As<IConfigurationRoot>();
            configurationMock.Setup(i => i.GetSection(LiteDbConfigurationConstants.CONFIGURATION_PREFIX))
                .Returns(dbSettingsMock.Object);
            configurationMock.Setup(i => i.GetSection(HashingConstants.CONFIGURATION_PREFIX))
                .Returns(hashingMock.Object);
            configurationMock.Setup(i => i.GetSection(SecurityConstants.CONFIGURATION_PREFIX))
                .Returns(securityMock.Object);

            var builder = new WebHostBuilder()
                .Configure(app => { })
                .ConfigureServices(services =>
                {
                    services.AddSingleton(provider => new Startup(configurationMock.Object));
                    services.AddSingleton(provider => configurationMock.Object);
                    services.AddSingleton<ILoggerFactory, LoggerFactory>();
                    services.AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>();
                    services.AddSingleton<IHostingEnvironment>(provider => new HostingEnvironment()
                    {
                        WebRootFileProvider = new Mock<IFileProvider>().Object
                    });
                    services
                        .AddSingleton<ILogger<CompiledPageRouteModelProvider>,
                            NullLogger<CompiledPageRouteModelProvider>>();
                    services.AddSingleton<DiagnosticSource>(new DiagnosticListener("test"));
                })
                .UseStartup<Startup>();

            // Act
            var server = new TestServer(builder);

            // Assert
            var serviceProvider = server.Host.Services;
            Assert.NotNull(server);
            Assert.NotNull(serviceProvider);
            Assert.NotNull(serviceProvider.GetService<IConfiguration>());
            Assert.NotNull(serviceProvider.GetService<IAntiforgery>());
            Assert.NotNull(serviceProvider.GetService<IApiVersionDescriptionProvider>());
            Assert.NotNull(serviceProvider.GetService<IApiVersionReader>());
            Assert.NotNull(serviceProvider.GetService<ISwaggerProvider>());

            var dbConfiguration = serviceProvider.GetService<LiteDbConfiguration>();
            Assert.NotNull(dbConfiguration);
            Assert.False(string.IsNullOrEmpty(dbConfiguration.DbPath));
            Assert.False(string.IsNullOrEmpty(dbConfiguration.Password));

            var hashingConfiguration = serviceProvider.GetService<HashingOptions>();
            Assert.NotNull(hashingConfiguration);
            Assert.True(hashingConfiguration.Iterations > 0);

            Assert.NotNull(serviceProvider.GetService<IDbFactory>());
            Assert.NotNull(serviceProvider.GetService<IUserRepository>());
            Assert.NotNull(serviceProvider.GetService<IDocumentRepository>());
            Assert.NotNull(serviceProvider.GetService<IPasswordHasher>());
            Assert.NotNull(serviceProvider.GetService<IUserService>());
            Assert.NotNull(serviceProvider.GetService<IDocumentService>());
            Assert.NotNull(serviceProvider.GetService<ITokenService>());
        }
    }
}
