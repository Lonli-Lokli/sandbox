﻿// Copyright (c) 2019 under MIT license.

using System.ComponentModel.DataAnnotations;

namespace CV.WebApi.V1.Contracts
{
    /// <summary>
    /// Request for user creation
    /// </summary>
    public class UserCreationRequest
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required, MinLength(1)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required, MinLength(1)]
        public string Password { get; set; }
    }
}
