﻿// Copyright (c) 2019 under MIT license.

using System.IO;
using System.Reflection;
using AutoMapper;
using CV.Domain.Configurations;
using CV.Domain.Services;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb;
using CV.Repository.LiteDb.Repositories;
using CV.WebApi.Extensions;
using CV.WebApi.Infrastructure;
using CV.WebApi.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.SwaggerGen;
using static Microsoft.AspNetCore.Mvc.CompatibilityVersion;

namespace CV.WebApi
{
    /// <summary>
    /// Startup class for the application.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services
                .AddAuthentication(CustomAuthSchemaDefaults.AuthenticationScheme)
                .AddScheme<CustomAuthSchemaOptions, CustomAuthSchemaHandler>(CustomAuthSchemaDefaults.AuthenticationScheme, null);
            services
                .AddMvc(mvcOptions =>
                {
                    mvcOptions.EnableEndpointRouting = false;
                    mvcOptions.ModelMetadataDetailsProviders.Add(new RequiredBindingMetadataProvider());
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    mvcOptions.Filters.Add(new AuthorizeFilter(policy));
                })
                .SetCompatibilityVersion(Version_2_2);

            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
                // reporting api versions will return the headers "api-supported-versions" and "api-deprecated-versions"
                options.ReportApiVersions = true;
            });
            services.AddVersionedApiExplorer(options =>
            {
                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                // note: the specified format code will format the version as "'v'major[.minor][-status]"
                options.GroupNameFormat = "'v'VVV";

                // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                // can also be used to control the format of the API version in route templates
                options.SubstituteApiVersionInUrl = true;
            });
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(options =>
            {
                // add a custom operation filter which sets default values
                options.OperationFilter<SwaggerDefaultValues>();

                // integrate xml comments
                options.IncludeXmlComments(XmlCommentsFilePath);
            });
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperDbToDmProfile());
                cfg.AddProfile(new AutoMapperDmToDbProfile());
                cfg.AddProfile(new AutoMapperWebApiToDmProfile());
                cfg.AddProfile(new AutoMapperDmToWebApiProfile());
            });
            mapperConfig.AssertConfigurationIsValid();
            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            // register validatable settings
            services.AddSetting<LiteDbConfiguration>(
                Configuration.GetSection(LiteDbConfigurationConstants.CONFIGURATION_PREFIX));
            services.AddSetting<HashingOptions>(
                Configuration.GetSection(HashingConstants.CONFIGURATION_PREFIX));
            services.AddSetting<SecurityOptions>(
                Configuration.GetSection(SecurityConstants.CONFIGURATION_PREFIX));

            services.AddSettingsValidatorActuator();

            services.AddSingleton<IDbFactory, DbFactory>();
            services.AddSingleton<IDocumentRepository, DocumentRepository>();
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IDocumentService, DocumentService>();
            services.AddSingleton<ITokenService, TokenService>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="provider">The provider.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseCors(builder =>
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // build a swagger endpoint for each discovered API version
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                    }
                });
        }

        private static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
