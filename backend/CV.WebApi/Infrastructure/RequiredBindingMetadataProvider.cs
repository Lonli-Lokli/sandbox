﻿// Copyright (c) 2019 under MIT license.

using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;

namespace CV.WebApi.Infrastructure
{
    /// <summary>
    /// Add possiblity to use <see cref="RequiredAttribute"/> as a <see cref="BindRequiredAttribute"/>.
    /// </summary>
    public class RequiredBindingMetadataProvider : IBindingMetadataProvider
    {
        /// <inheritdoc />
        public void CreateBindingMetadata(BindingMetadataProviderContext context)
        {
            if (context.PropertyAttributes?.OfType<RequiredAttribute>().Any() == true)
            {
                context.BindingMetadata.IsBindingRequired = true;
            }
        }
    }
}
