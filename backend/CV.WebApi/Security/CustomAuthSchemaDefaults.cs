﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Security
{
    internal class CustomAuthSchemaDefaults
    {
        public const string AuthenticationScheme = "Bearer";
    }
}
