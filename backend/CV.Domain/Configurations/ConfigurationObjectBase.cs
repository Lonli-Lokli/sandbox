﻿// Copyright (c) 2019 under MIT license.

using System.ComponentModel.DataAnnotations;

namespace CV.Domain.Configurations
{
    /// <summary>
    /// Represents self-validatable entity.
    /// </summary>
    public interface IValidatable
    {
        /// <summary>
        /// Validates entity.
        /// </summary>
        void Validate();
    }

    /// <inheritdoc />
    public class ConfigurationObjectBase : IValidatable
    {
        /// <inheritdoc />
        public void Validate()
        {
            Validator.ValidateObject(this, new ValidationContext(this, null, null), true);
        }
    }
}
