﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Linq;
using System.Security.Cryptography;
using CV.Domain.Configurations;
using CV.Domain.Services.Abstractions;

namespace CV.Domain.Services
{
    /// <inheritdoc />
    public sealed class PasswordHasher : IPasswordHasher
    {
        private readonly HashingOptions _options;
        private const int SaltSize = 16;
        private const int KeySize = 32;

        /// <inheritdoc />
        public PasswordHasher(HashingOptions options)
        {
            _options = options;
        }

        /// <inheritdoc />
        public string Hash(string password)
        {
            using (var algorithm = new Rfc2898DeriveBytes(
                password,
                SaltSize,
                _options.Iterations,
                HashAlgorithmName.SHA256))
            {
                var key = Convert.ToBase64String(algorithm.GetBytes(KeySize));
                var salt = Convert.ToBase64String(algorithm.Salt);

                return $"{_options.Iterations}.{salt}.{key}";
            }
        }

        /// <inheritdoc />
        public bool Check(string hash, string password)
        {
            var parts = hash.Split('.', 3);
            if (parts.Length != 3)
            {
                throw new FormatException("Unexpected hash format.");
            }

            var iterations = Convert.ToInt32(parts[0]);
            var salt = Convert.FromBase64String(parts[1]);
            var key = Convert.FromBase64String(parts[2]);

            using (var algorithm = new Rfc2898DeriveBytes(
                password,
                salt,
                iterations,
                HashAlgorithmName.SHA256))
            {
                var keyToCheck = algorithm.GetBytes(KeySize);

                return keyToCheck.SequenceEqual(key);
            }
        }
    }
}
