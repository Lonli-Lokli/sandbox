﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CV.Domain.Models;

namespace CV.Domain.Services.Abstractions
{
    /// <summary>
    /// User service.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        IReadOnlyCollection<ValidationResult> CreateUser(string userName, string password);

        /// <summary>
        /// Validates user\password for the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        bool ValidateUserPassword(string userName, string password);

        /// <summary>
        /// Determines whether user name can be taken.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        bool IsNameAvailable(string userName);
    }
}
